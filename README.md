# Geojson Antimeridian Cutting

[![Crates.io](https://img.shields.io/crates/v/geojson-antimeridian-cut)][cratesio]
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/avandesa/geojson-antimeridian-cut-rs)][pipeline]

## GeoJSON

[GeoJSON][rfc] is a standard for representing geographic data in a JSON file. `Features` and
`FeatureCollections` are composed of different geometry objects, including:

* `LineString`s
* `MultiLineString`s
* `Polygon`s
* `MultiPolygon`s
* `GeometryCollection`s

It is very likely that some geographic data may cross the Antimeridian (180° E or 180° W). [RFC
7946 Section 3.1.9][split_section] specifies that such objects SHOULD be broken up into two or
more objects, none of which cross the antimeridian, and which together are all equivalent. This
crate implements that splitting.

The types in this crate are re-exported from the [`geojson` crate][crate], and the functions
implement the relevant splitting algorithms for each type. If a `GeoJSON` object does not cross the antimeridian, `None` is returned, otherwise a copy of the object with the relevant splits is returned.

## Example

```rust
let given: Geometry = json!({
  "type": "LineString",
  "coordinates": [[170, 0], [-170, 0]],
})
.try_into()
.unwrap();

let expected: Geometry = json!({
  "type": "MultiLineString",
  "coordinates": [
      [[170, 0], [180, 0]],
      [[-180, 0], [-170, 0]],
  ],
})
.try_into()
.unwrap();

let result = split_geometry(&given);
assert_eq!(Some(expected), result);
```

[rfc]: https://tools.ietf.org/html/rfc7946
[split_section]: https://tools.ietf.org/html/rfc7946#section-3.1.9
[crate]: https://crates.io/crates/geojson
[cratesio]: https://crates.io/crates/geojson-antimeridian-cut
[pipeline]: https://gitlab.com/avandesa/geojson-antimeridian-cut-rs/pipelines
