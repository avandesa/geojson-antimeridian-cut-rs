#![deny(clippy::all)]

pub use geojson::{Feature, FeatureCollection, GeoJson, Geometry};

mod split;
pub use split::split_geojson;

mod feature;
pub use feature::split_feature;

mod geometry;
pub use geometry::split_geometry;

mod collection;
pub use collection::split_feature_collection;

mod util;
mod lines;
