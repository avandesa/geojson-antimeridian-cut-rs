use crate::util::{antimeridian_intersect, straddles_antimeridian};

use geojson::{LineStringType, Position, Value};

/// If a `LineString` crosses the antimeridian at least one time, breaks the `LineString` into a
/// `MultiLineString`.
pub fn split_line_string(line: LineStringType) -> Value {
    if let Some(split) = split_coordinate_array(&line) {
        Value::MultiLineString(split)
    } else {
        Value::LineString(line)
    }
}

/// Examines each substring in the given `MultiLineString`. If any of them cross the antimeridian,
/// returns a new `MultiLineString` with each crossing lines split up in-place
pub fn split_multi_line_string(lines: Vec<LineStringType>) -> Value {
    Value::MultiLineString(
        lines
            .iter()
            .map(|line| {
                if let Some(split) = split_coordinate_array(&line) {
                    split
                } else {
                    vec![line.to_vec()]
                }
            })
            .flatten()
            .collect(),
    )
}

/// Performs the splitting of a line string given a pre-calculated list of crossing indices
/// If the line does not cross the antimeridian, `None` is returned.
fn split_coordinate_array(coordinates: &[Position]) -> Option<Vec<LineStringType>> {
    let crossings = find_crossing_points(coordinates);

    if crossings.is_empty() {
        return None;
    }

    let mut rtn: Vec<LineStringType> = Vec::with_capacity(crossings.len() + 1);

    // Add the first segment
    rtn.push(coordinates[0..=crossings[0]].to_vec());
    // Add the middle segments
    crossings
        .windows(2)
        .map(|slice| (&slice[0], &slice[1]))
        .for_each(|(left_bound, right_bound)| {
            // let () = (left_bound, right_bound);
            rtn.push(coordinates[*left_bound + 1..=*right_bound].to_vec());
        });
    // Add the last segment
    rtn.push(coordinates[crossings[crossings.len() - 1] + 1..].to_vec());

    debug_assert_eq!(coordinates.len(), rtn.iter().fold(0, |l, s| l + s.len()));

    for i in 1..rtn.len() {
        let left = rtn.get(i - 1).unwrap();
        let right = rtn.get(i).unwrap();

        let left_last = left.last().unwrap();
        let right_first = right.first().unwrap();

        let intersect_lat = antimeridian_intersect(left_last, right_first);

        let left_intersect = 180_f64.copysign(left_last[0]);
        let right_intersect = 180_f64.copysign(right_first[0]);

        rtn[i - 1].push(vec![left_intersect, intersect_lat]);
        rtn[i].insert(0, vec![right_intersect, intersect_lat]);
    }

    Some(rtn)
}

/// Given a `LineString`, finds all indicies in the array, if any, at which the line crosses the
/// antimeridian.
fn find_crossing_points(coordinates: &[Position]) -> Vec<usize> {
    coordinates
        .windows(2)
        .enumerate()
        .filter(|(_, slice)| {
            let a = &slice[0][0];
            let b = &slice[1][0];

            straddles_antimeridian(*a, *b)
        })
        .map(|(i, _)| i)
        .collect()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn split_array_1() {
        let given = [vec![170.0, 0.0], vec![-170.0, 0.0]];
        #[rustfmt::skip]
        let expected: Vec<LineStringType> = vec![
            vec![
                vec![170.0, 0.0],
                vec![180.0, 0.0],
            ],
            vec![
                vec![-180.0, 0.0],
                vec![-170.0, 0.0],
            ],
        ];
        let result = split_coordinate_array(&given).unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn split_array_2() {
        #[rustfmt::skip]
        let given = [
            vec![-170.0, 20.0],
            vec![170.0, 0.0],
            vec![-170.0, -20.0],
        ];
        #[rustfmt::skip]
        let expected = vec![
            vec![
                vec![-170.0, 20.0],
                vec![-180.0, 10.0],
            ],
            vec![
                vec![180.0, 10.0],
                vec![170.0, 0.0],
                vec![180.0, -10.0],
            ],
            vec![
                vec![-180.0, -10.0],
                vec![-170.0, -20.0],
            ],
        ];
        let result = split_coordinate_array(&given).unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn crossings() {
        let given = [vec![1.0, 0.0], vec![10.0, 0.0]];
        assert!(find_crossing_points(&given).is_empty());

        let given = [vec![-170.0, 0.0], vec![170.0, 0.0]];
        let expected = vec![0];
        assert_eq!(find_crossing_points(&given), expected);

        let given = [
            vec![-170.0, -20.0],
            vec![170.0, -20.0],
            vec![170.0, 20.0],
            vec![-170.0, 20.0],
            vec![-170.0, -20.0],
        ];
        let expected = vec![0, 2];
        assert_eq!(find_crossing_points(&given), expected);
    }
}
