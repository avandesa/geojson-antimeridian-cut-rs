use geojson::{GeoJson, GeoJson::*};

use crate::{split_feature, split_feature_collection, split_geometry};

pub fn split_geojson(item: GeoJson) -> GeoJson {
    match item {
        Geometry(geom) => GeoJson::Geometry(split_geometry(geom)),
        Feature(feat) => GeoJson::Feature(split_feature(feat)),
        FeatureCollection(fc) => GeoJson::FeatureCollection(split_feature_collection(fc)),
    }
}
