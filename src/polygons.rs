use geojson::{PolygonType, Value, Position};

pub fn split_polygon(_poly: PolygonType) -> Value {
    unimplemented!()
}

pub fn split_multi_polygon(_polygons: Vec<PolygonType>) -> Value {
    unimplemented!()
}

fn split_linear_ring(ring: Vec<Position>) -> Vec<Vec<Position>> {
    unimplemented!()
}
