/// Determines if two points straddle the antimeridian.
///
/// Points straddle the antimeridian iff
/// * The difference in longitude is > 360 degrees
/// * OR The shortest line drawn between them crosses teh antimeridian
pub fn straddles_antimeridian(a: f64, b: f64) -> bool {
    if (a - b).abs() > 360.0 {
        return true;
    }

    let a = within_bounds(a);
    let b = within_bounds(b);

    if a > 0.0 {
        b < -(180.0 - a)
    } else {
        a < -(180.0 - b)
    }
}

/// Brings a longitude to an equivalent lon. long in the range [-180, 180]
fn within_bounds(lon: f64) -> f64 {
    let lon = lon % 360_f64;

    if lon > 180_f64 {
        lon - 360_f64
    } else if lon < -180_f64 {
        lon + 360_f64
    } else {
        lon
    }
}

/// Calculates the latitude at which a line drawn between two coordinates would intersect the
/// antimeridian. Assumes that, for both `a` and `b`,
/// * -180 < lon < 180
/// * -90 < lat < 90
/// * `a` and `b` straddle the antimeridian
pub fn antimeridian_intersect(a: &[f64], b: &[f64]) -> f64 {
    let lon_a = make_lon_positive(a[0]);
    let lon_b = make_lon_positive(b[0]);

    let slope = (b[1] - a[1]) / (lon_b - lon_a);

    slope * (180.0 - lon_a) + a[1]
}

/// Returns an equivalent longitude > 0
fn make_lon_positive(mut lon: f64) -> f64 {
    while lon < 0.0 {
        lon += 360.0;
    }

    lon
}

#[cfg(test)]
mod test {
    use super::*;
    use std::f64::EPSILON;

    #[test]
    fn straddle() {
        assert!(straddles_antimeridian(-170.0, 170.0));
        assert!(straddles_antimeridian(0.0, 400.0));
        assert!(straddles_antimeridian(400.0, 0.0));
        assert!(straddles_antimeridian(-170.0, -190.0));
        assert!(straddles_antimeridian(170.0, 190.0));
        assert!(!straddles_antimeridian(0.0, 10.0));
        assert!(!straddles_antimeridian(170.0, 180.0));
        assert!(!straddles_antimeridian(-170.0, -180.0));
    }

    #[test]
    fn intersect() {
        let a = [-170.0, 10.0];
        let b = [170.0, -10.0];
        assert!(dbg!(antimeridian_intersect(&a, &b)) <= EPSILON);

        let a = [-1.0, 10.0];
        let b = [1.0, 10.0];
        let result = dbg!(antimeridian_intersect(&a, &b));

        assert!((result - 10.0).abs() <= EPSILON);

        let a = [172.66, -43.51]; // Christchurch
        let b = [-149.59, -17.51];
        let result = dbg!(antimeridian_intersect(&a, &b));

        assert!((result - -38.45).abs() <= 0.01);
    }

    #[test]
    fn bring_within_bounds_no_change() {
        assert!(within_bounds(0.0).abs() <= EPSILON);

        assert!((within_bounds(1.0) - 1.0).abs() <= EPSILON);
        assert!((within_bounds(-1.0) - -1.0).abs() <= EPSILON);

        assert!((within_bounds(180.0) - 180.0).abs() <= EPSILON);
        assert!((within_bounds(-180.0) - -180.0).abs() <= EPSILON);
    }

    #[test]
    fn bring_within_bounds() {
        assert!((within_bounds(181.0) - -179.0).abs() <= EPSILON);
        assert!((within_bounds(-181.0) - 179.0).abs() <= EPSILON);

        assert!(within_bounds(360.0) <= EPSILON);
        assert!(within_bounds(-360.0) <= EPSILON);
    }
}
