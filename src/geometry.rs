use crate::lines::{split_line_string, split_multi_line_string};

use geojson::{Geometry, PolygonType, Value};

pub fn split_geometry(geometry: Geometry) -> Geometry {
    let value = match geometry.value {
        Value::LineString(ls) => split_line_string(ls),
        Value::MultiLineString(lines) => split_multi_line_string(lines),
        Value::Polygon(poly) => split_polygon(poly),
        Value::MultiPolygon(mp) => split_multi_polygon(mp),
        Value::GeometryCollection(gc) => split_geometry_collection(gc),
        _ => geometry.value, // Points & MultiPoints
    };

    Geometry {
        value,
        bbox: geometry.bbox,
        foreign_members: geometry.foreign_members,
    }
}

fn split_polygon(_poly: PolygonType) -> Value {
    unimplemented!()
}

fn split_multi_polygon(_polygons: Vec<PolygonType>) -> Value {
    unimplemented!()
}

fn split_geometry_collection(_gc: Vec<Geometry>) -> Value {
    unimplemented!()
}

#[cfg(test)]
mod test {
    use super::split_geometry;
    use {geojson::Geometry, serde_json::json, std::convert::TryInto};

    mod lines {
        use super::*;

        #[test]
        fn no_split_simple_line_string() {
            let given: Geometry = json!({
                "type": "LineString",
                "coordinates": [[0, 0], [1, 1]],
            })
            .try_into()
            .unwrap();

            let expected = given.clone();
            let result = split_geometry(given);

            assert_eq!(expected, result);
        }

        #[test]
        fn split_one_crossing() {
            let given: Geometry = json!({
                "type": "LineString",
                "coordinates": [[170, 0], [-170, 0]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[170, 0], [180, 0]],
                    [[-180, 0], [-170, 0]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_one_crossing_reverse() {
            let given: Geometry = json!({
                "type": "LineString",
                "coordinates": [[-170, 0], [170, 0]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[-170, 0], [-180, 0]],
                    [[180, 0], [170, 0]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_two_crossings() {
            let given: Geometry = json!({
                "type": "LineString",
                "coordinates": [
                    [-170, 20],
                    [170, 0],
                    [-170, -20],
                ],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[-170, 20], [-180, 10]],
                    [[180, 10], [170, 0], [180, -10]],
                    [[-180, -10], [-170, -20]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }
    }

    mod multi_lines {
        use super::*;

        #[test]
        fn no_split_simple_mls() {
            let given: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[1, 1], [2, 2]],
                    [[-1, 0], [1, 1], [-1, 2]],
                ],
            })
            .try_into()
            .unwrap();

            let expected = given.clone();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_one_of_two() {
            let given: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[-1, 0], [1, 0]],
                    [[170, 0], [-170, 0]],

                ],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[-1, 0], [1, 0]],
                    [[170, 0], [180, 0]],
                    [[-180, 0], [-170, 0]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn cut_both() {
            let given: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[-160, 30], [160, 20]],
                    [[170, 0], [-170, 0]],

                ],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiLineString",
                "coordinates": [
                    [[-160, 30], [-180, 25]],
                    [[180, 25], [160, 20]],
                    [[170, 0], [180, 0]],
                    [[-180, 0], [-170, 0]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }
    }

    mod polygons {
        use super::*;

        #[test]
        fn no_split_simple() {
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [[
                    [40, 50],
                    [50, 50],
                    [50, 40],
                    [40, 40],
                    [40, 50],
                ]],
            })
            .try_into()
            .unwrap();

            let expected = given.clone();

            let result = split_geometry(given);
            assert_eq!(expected, result);
        }

        #[test]
        fn split_square() {
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [[
                    [170, -10],
                    [-170, -10],
                    [-170, 10],
                    [170, 10],
                    [170, -10],
                ]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [[
                            [180, 10],
                            [170, 10],
                            [170, -10],
                            [180, -10],
                            [180, 10],
                    ]],
                    [[
                            [-180, -10],
                            [-170, -10],
                            [-170, 10],
                            [-180, 10],
                            [-180, -10],
                    ]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result);
        }

        #[test]
        fn split_first_after_crossing() {
            // Split a polygon where the crossing occurs immediately before the first point.
            // Apparently it was an issue in the npm package.
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [[
                    [-170, -10],
                    [-170, 10],
                    [170, 10],
                    [170, -10],
                    [-170, -10],
                ]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [[
                            [180, 10],
                            [170, 10],
                            [170, -10],
                            [180, -10],
                            [180, 10],
                    ]],
                    [[
                            [-180, -10],
                            [-170, -10],
                            [-170, 10],
                            [-180, 10],
                            [-180, -10],
                    ]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_reverse() {
            // Though linear rings should follow the right-hand rule, we should be able to properly
            // cut rings that go clockwise so we can cut holes properly
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [[
                    [-170, -10],
                    [170, -10],
                    [170, 10],
                    [-170, 10],
                    [-170, -10],
                ]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [[
                        [-180, 10],
                        [-170, 10],
                        [-170, -10],
                        [-180, -10],
                        [-180, 10],
                    ]],
                    [[
                        [180, -10],
                        [170, -10],
                        [170, 10],
                        [180, 10],
                        [180, -10],
                    ]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_irregular() {
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [[
                    [175, 10],
                    [-160, -10],
                    [-160, 10],
                    [175, -10],
                    [175, 10],
                ]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [[
                            [-180, 6],
                            [-160, -10],
                            [-160, 10],
                            [-180, -6],
                            [-180, 6],
                    ]],
                    [[
                            [180, -6],
                            [175, -10],
                            [175, 10],
                            [180, 6],
                            [180, -6],
                    ]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_many_crossings() {
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [[
                    [179, 4],
                    [178, 0],
                    [-179, 0],
                    [179, 2],
                    [-179, 4],
                    [-178, 8],
                    [179, 8],
                    [-179, 6],
                    [179, 4],
                ]],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [[
                        [180, 8],
                        [179, 8],
                        [180, 7],
                        [180, 8],
                    ]],
                    [[
                        [180, 1],
                        [179, 2],
                        [180, 3],
                        [180, 5],
                        [179, 4],
                        [178, 0],
                        [180, 0],
                        [180, 1],
                    ]],
                    [[
                        [-180, 0],
                        [-179, 0],
                        [-180, 1],
                        [-180, 0],
                    ]],
                    [[
                        [-180, 3],
                        [-179, 4],
                        [-178, 8],
                        [-180, 8],
                        [-180, 7],
                        [-179, 6],
                        [-180, 5],
                        [-180, 3],
                    ]],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result);
        }
    }

    mod poly_holes {
        use super::*;

        #[test]
        fn no_split_with_hole() {
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [
                    [
                        [0, 0],
                        [3, 0],
                        [3, 3],
                        [0, 3],
                        [0, 0],
                    ],
                    [
                        [1, 1],
                        [1, 2],
                        [2, 2],
                        [2, 1],
                        [1, 1],
                    ],
                ],
            })
            .try_into()
            .unwrap();

            let expected = given.clone();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_poly_not_hole() {
            let hole = json!([[170, 10], [170, 0], [175, 0], [175, 10], [170, 10]]);

            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [
                    [
                        [-160, -20],
                        [160, -20],
                        [160, 20],
                        [-160, 20],
                        [-160, -20],
                    ],
                    hole,
                ],

            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [-180, 20],
                            [-160, 20],
                            [-160, -20],
                            [-180, -20],
                            [-180, 20],
                        ],
                        hole,
                    ],
                    [
                        [
                            [180, -20],
                            [160, -20],
                            [160, 20],
                            [180, 20],
                            [180, -20],
                        ],
                        hole,
                    ],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_hole_not_poly() {
            let poly = json!([[160, 20], [160, 0], [175, 0], [175, 20], [160, 20]]);

            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [
                    poly,
                    [
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                        [170, -10],
                        [-170, -10],
                    ],
                ],
            })
            .try_into()
            .unwrap();

            let expected: Geometry = json!({
                "type": "Polygon",
                "coordinates": [
                    poly,
                    [
                        [180, 10],
                        [170, 10],
                        [170, -10],
                        [180, -10],
                        [180, 10],
                    ],
                    [
                        [-180, -10],
                        [-170, -10],
                        [-170, 10],
                        [-180, 10],
                        [-180, -10],
                    ],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);
            assert_eq!(expected, result)
        }

        #[test]
        fn split_poly_and_hole() {
            let given: Geometry = json!({
                "type": "Polygon",
                "coordinates": [
                    [
                        [-160, -20],
                        [160, -20],
                        [160, 20],
                        [-160, 20],
                        [-160, -20],
                    ],
                    [
                        [170, 10],
                        [170, -10],
                        [-170, -10],
                        [-170, 10],
                        [170, 10],
                    ],
                ],
            })
            .try_into()
            .unwrap();

            #[rustfmt::skip]
            let expected_hole = json!([
                [[180, 10], [170, 10], [170, -10], [180, -10], [180, 10],],
                [[-180, -10], [-170, -10], [-170, 10], [-180, 10], [-180, -10]],
            ]);

            let expected: Geometry = json!({
                "type": "MultiPolygon",
                "coordinates": [
                    [
                        [
                            [-180, 20],
                            [-160, 20],
                            [-160, -20],
                            [-180, -20],
                            [-180, 20],
                        ],
                        expected_hole[0],
                        expected_hole[1],
                    ],
                    [
                        [
                            [180, -20],
                            [160, -20],
                            [160, 20],
                            [180, 20],
                            [180, -20],
                        ],
                        expected_hole[0],
                        expected_hole[1],
                    ],
                ],
            })
            .try_into()
            .unwrap();

            let result = split_geometry(given);

            assert_eq!(expected, result);
        }
    }
}
